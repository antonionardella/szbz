Write-Output "Disable Windows Update Server"
Set-ItemProperty -Path HKLM:SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate\AU -Name UseWUServer -Value 0

Write-Output "Restarting Windows Update Service"
Restart-Service -Name wuauserv -Force

Write-Output "Install the OpenSSH Server"
Add-WindowsCapability -Online -Name OpenSSH.Server~~~~0.0.1.0

Write-Output "Start-Service sshd"
Set-Service -Name sshd -StartupType 'Automatic'


Write-Output "Confirm the Firewall rule is configured. It should be created automatically by setup. Run the following to verify"
if (!(Get-NetFirewallRule -Name "OpenSSH-Server-In-TCP" -ErrorAction SilentlyContinue | Select-Object Name, Enabled)) {
    Write-Output "Firewall Rule 'OpenSSH-Server-In-TCP' does not exist, creating it..."
    New-NetFirewallRule -Name 'OpenSSH-Server-In-TCP' -DisplayName 'OpenSSH Server (sshd)' -Enabled True -Direction Inbound -Protocol TCP -Action Allow -LocalPort 22 -Profile Domain, Private
} else {
    Write-Output "Firewall rule 'OpenSSH-Server-In-TCP' has been created and exists."
}

Write-Output "Add Tony public key"
$authorizedKey="ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAINlbbqfhzC6RuBI2/LElrnOmbcB4uXxA8uqki1KZye3U antonio.nardella@provincia.bz.it"
Add-Content -Force -Path $env:ProgramData\ssh\administrators_authorized_keys -Value $authorizedKey;icacls.exe ""$env:ProgramData\ssh\administrators_authorized_keys"" /inheritance:r /grant ""Administrators:F"" /grant ""SYSTEM:F""

Write-Output "Set Windows Update Server Key to 1"
Set-ItemProperty -Path HKLM:SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate\AU -Name UseWUServer -Value 1

Write-Output "Restart Windows Update Service"
Restart-Service -Name wuauserv -Force
