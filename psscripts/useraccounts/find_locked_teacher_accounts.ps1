# Import the Active Directory module
Import-Module ActiveDirectory

# Get all users and filter for disabled, blocked, or expired accounts
try{
    $searchbasedomain = "OU=Teachers,OU=DidaNet,$(Get-ADDomain)"
    $userlist = Get-ADUser -Filter 'Enabled -eq $false' -SearchBase $searchbasedomain -Properties SamAccountName,Enabled,PasswordExpired,AccountExpirationDate |
        Where-Object { $_.PasswordExpired -eq $true -or $_.AccountExpirationDate -lt (Get-Date) }

} catch {
    Write-Host "No user to list"
}

# Display the results
foreach ($user in $userlist) {
    Write-Host "Username: $($user.samaccountname)"
    Write-Host "User: $($user.Name)"
    Write-Host "Enabled: $($user.Enabled)"
    Write-Host "Password Expired: $($user.PasswordExpired)"
    Write-Host "Account Expires: $($user.AccountExpirationDate)"
    Write-Host "-----------------------------"
    
    # Unlock the account
    try{
        $username = $user.samaccountname
        Unlock-ADAccount -Identity $username
        Enable-ADAccount -Identity $username
    } catch {
            Write-Host "Error unlocking the account: $_"
        }
    Write-Host "Account unlocked or enabled"
}

# Clean up the module import (optional)
Remove-Module ActiveDirectory
