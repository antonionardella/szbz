## Use with unlock_account.ps1 -username theusernameoftheuser

param (
    [string]$username
)

# Function to check and update user account properties
function Manage-ADUserAccount {
    param (
        [string]$username
    )

    try {
        # Retrieve the user account
        $user = Get-ADUser -Filter {name -like $username -or samaccountname -like $username}
        $name = $user | select Name
        Write-Host $username
        Write-Host $user
    } catch {
        Write-Host "Error: User '$username' not found in Active Directory."
        return
    }

    # Display current account status
    Write-Host "User $name Account Status:"
    if ($user.AccountIsLockedOut) {
        Write-Host "Blocked: $($user.AccountIsLockedOut)"
    } else {
        Write-Host "Blocked: No"
    }
    Write-Host "Enabled: $($user.Enabled)"
    Write-Host "Password Expired: $($user.PasswordExpired)"
    if ($user.AccountExpirationDate) {
        Write-Host "Account Expires: $($user.AccountExpirationDate)"
    } else {
        Write-Host "Account Expires: Never"
    }

    # Provide options to manage the account
    $choice = Read-Host "Select an option:`n1. Unlock Account`n2. Reset Password`n3. Set Account Expiration to 'Never'`nEnter 0 to exit."

    switch ($choice) {
        1 { 
            try{
            Unlock-ADAccount -Identity $username
            Enable-ADAccount -Identity $username
            } catch {
                Write-Host "Error unlocking the account: $_"
            }
          }
        2 {
            $newPassword = Read-Host "Enter a new password for $username" -AsSecureString
            Set-ADAccountPassword -Identity $username -NewPassword $newPassword -Reset
        }
        3 { Clear-ADAccountExpiration -Identity $username }
        0 { break }
        default { Write-Host "Invalid choice. Exiting." }
    }
}

# Connect to Active Directory
try {
    Import-Module ActiveDirectory
} catch {
    Write-Host "Error: Active Directory module not found. Please ensure the module is installed."
    exit
}

# Call the function to manage the user account
Manage-ADUserAccount -username $username
