## Copy me to "C:\Users\<your username>\Documents\WindowsPowerShell\Microsoft.PowerShell_profile.ps1"
# Set an alias for ll
Set-Alias ll dir 
# Set a function to get an ssh alias
function sshtstsrv { 
    ssh administrator@10.0.10.35
}
# Set the alias using the function above
Set-Alias -name sshtestserver -value sshtstsrv